# posh-npm-upgrade
[![PowerShell Gallery](https://img.shields.io/powershellgallery/v/npm-upgrade.svg?style=flat-square)](https://www.powershellgallery.com/packages/npm-upgrade)
[![PowerShell Gallery](https://img.shields.io/powershellgallery/dt/npm-upgrade.svg?style=flat-square)](https://www.powershellgallery.com/packages/npm-upgrade)

A [PowerShell](https://microsoft.com/powershell) module for upgrading [npm](https://www.npmjs.com/) on Windows.

## ⚙️ Installation

```powershell
Install-Module npm-upgrade
```

## 🛂 Usage

You have to import the module to use `npm-upgrade`.

Add below command into your PowerShell profile.

```powershell
Import-Module npm-upgrade
```

Then restart your PowerShell.

---

Now, you can use below commands to upgrade your npm to lastest version.

```powershell
Update-NPM

# Your can also use these aliases instead.
Upgrade-NPM
npmu
```
